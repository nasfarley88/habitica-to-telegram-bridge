import os
import pdb
import logging

from flask import Flask, request
import requests


logging.basicConfig(level=logging.INFO)
logger = logging.getLogger(__name__)


app = Flask(__name__)


@app.route("/", methods=['POST'])
def bridge():
    # Let's check that all the environment variables are there. This will
    # except if any are not found
    tg_bot_token = os.environ['HABITICA_TG_BOT_TOKEN']
    tg_group_id = os.environ['HABITICA_TG_GROUP']

    try:
        data_to_send_to_telegram = {
            'chat_id': tg_group_id,
            'text': '_' + request.json['chat']['user'] + ':_ ' + request.json['chat']['text'],
            'parse_mode': 'Markdown'
        }
    except KeyError:
        data_to_send_to_telegram = {
            'chat_id': tg_group_id,
            'text': request.json['chat']['text'],
            'parse_mode': 'Markdown'
        }

    tg_response = requests.post(
        f'https://api.telegram.org/bot{tg_bot_token}/sendMessage',
        data=data_to_send_to_telegram,
    )

    return str(tg_response)


if __name__ == '__main__':
    app.run(debug=False, host='0.0.0.0', port=8200) #run app in debug mode on port 5000
